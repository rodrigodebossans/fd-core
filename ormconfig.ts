/* ALL SETTINGS IN THIS FILE ARE OVERWRITTEN BY THE VARIABLES CONTAINED IN .ENV */
export = {
  type: 'your-configuration',
  host: 'your-configuration',
  port: null,
  username: 'your-configuration',
  password: 'your-configuration',
  database: 'your-configuration',
  synchronize: null,
  logging: null,
  entities: ['your-configuration'],
  migrations: ['your-configuration'],
  subscribers: ['your-configuration'],
  cli: {
    entitiesDir: 'your-configuration',
    migrationsDir: 'your-configuration',
    subscribersDir: 'your-configuration',
  },
};
