import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  UpdateDateColumn,
  CreateDateColumn,
  Column,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import {
  MaxLength,
  IsNotEmpty,
  ValidationError,
  validate,
} from 'class-validator';

import { Partition } from '@model/partition/partition';
import { UninformedObjectException } from '@exception/uninformed-object.exception';

@Entity('directory')
@Tree('materialized-path')
export class Directory {
  @PrimaryGeneratedColumn()
  public id?: number;

  @MaxLength(143, {
    message:
      'The $property property is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @IsNotEmpty({
    message: 'The $property property is required',
  })
  @Column()
  public name: string;

  @TreeParent()
  public parentDirectory?: Directory;

  @TreeChildren()
  public childDirectoryList?: Directory[];

  @ManyToOne((type) => Partition, (partition) => partition.directoryList, {
    onDelete: 'CASCADE',
  })
  @IsNotEmpty({
    message: 'The $property property is required',
  })
  public partition: Partition;

  @CreateDateColumn()
  public creationDate?: Date;

  @UpdateDateColumn()
  public modifiedDate?: Date;

  public static fromObj(obj: any): Directory {
    if (obj) {
      const directory: Directory = new Directory();

      if (obj['id']) directory.id = obj['id'];

      directory.name = obj['name'];

      if (obj['parentDirectory'])
        directory.parentDirectory = Directory.fromObj(obj['parentDirectory']);

      if (obj['childDirectoryList'])
        directory.childDirectoryList = Directory.fromArray(
          obj['childDirectoryList']
        );

      directory.partition = Partition.fromObj(obj['partition']);

      if (obj['creationDate']) directory.creationDate = obj['creationDate'];

      if (obj['modifiedDate']) directory.modifiedDate = obj['modifiedDate'];

      return directory;
    }
  }

  public static fromArray(array: any[]): Directory[] {
    return array.map((obj: any) => Directory.fromObj(obj));
  }

  public static async validate(directory: Directory): Promise<void> {
    const validationErrors: ValidationError[] = await validate(directory);
    if (validationErrors.length)
      throw new UninformedObjectException(validationErrors);
  }
}
