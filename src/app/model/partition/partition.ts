import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  Column,
} from 'typeorm';
import { Directory } from '../directory/directory';
import {
  MaxLength,
  IsNotEmpty,
  ValidationError,
  validate,
} from 'class-validator';
import { UninformedObjectException } from '../../exception/uninformed-object.exception';

@Entity('partition')
export class Partition {
  @PrimaryGeneratedColumn()
  public id?: number;

  @MaxLength(143, {
    message:
      'The $property property is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @IsNotEmpty({
    message: 'The $property property is required',
  })
  @Column()
  public name: string;

  @OneToMany((type) => Directory, (user) => user.partition, {
    onDelete: 'CASCADE',
  })
  public directoryList?: Directory[];

  @CreateDateColumn()
  public creationDate?: Date;

  @UpdateDateColumn()
  public modifiedDate?: Date;

  private _idsCheck: number[] = [];
  private _existingIds: number[] = [];

  public static fromObj(obj: any): Partition {
    if (obj) {
      const partition: Partition = new Partition();

      if (obj['id']) partition.id = obj['id'];
      partition.name = obj['name'];

      if (obj['directoryList']) partition.directoryList = obj['directoryList'];
      else partition.directoryList = [];

      if (obj['creationDate']) partition.creationDate = obj['creationDate'];
      if (obj['modifiedDate']) partition.modifiedDate = obj['modifiedDate'];

      partition._idsCheck = undefined;
      partition._existingIds = undefined;

      return partition;
    }
  }

  public static fromArray(array: any[]): Partition[] {
    return array.map((obj: any) => Partition.fromObj(obj));
  }

  public static async validate(partition: Partition): Promise<void> {
    const partitionErrors: ValidationError[] = await validate(partition);
    if (partitionErrors.length)
      throw new UninformedObjectException(partitionErrors);
  }

  public checkDirectoryListAndRemovesDuplicates(): void {
    this.directoryList.forEach((directory: Directory) => {
      this.populateDirectoryTreeIDs(directory);
    });

    this.directoryList = this.directoryList.filter(
      (directory: Directory) => !this._existingIds.includes(directory.id)
    );

    this._idsCheck = [];
    this._existingIds = [];
  }

  private populateDirectoryTreeIDs(directory: Directory): void {
    if (!this._idsCheck.find((id) => id === directory.id))
      this._idsCheck.push(directory.id);
    else {
      if (!this._existingIds.find((id) => id === directory.id))
        this._existingIds.push(directory.id);
    }

    if (directory.childDirectoryList.length > 0) {
      directory.childDirectoryList.forEach((directoryChild: Directory) => {
        this.populateDirectoryTreeIDs(directoryChild);
      });
    }
  }
}
