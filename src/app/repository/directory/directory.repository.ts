import { EntityRepository, EntityManager } from 'typeorm';
import { Directory } from '@model/directory/directory';

@EntityRepository()
export class DirectoryRepository {
  constructor(private manager: EntityManager) {}

  public async removeDirectoriesCascade(
    directoryList: Directory[]
  ): Promise<void> {
    const orderedIdsDesc: number[] = directoryList
      .map((directory: Directory) => directory.id)
      .sort((a: number, b: number) => (a < b ? 1 : a > b ? -1 : 0));

    for (const id of orderedIdsDesc) {
      await this.manager.delete(Directory, id);
    }
  }
}
