import 'reflect-metadata';
import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import controllers from './controller';
import { createConnection } from 'typeorm';
import cors from 'cors';

class App {
  public express: express.Application;

  constructor() {
    dotenv.config();
    this.express = express();
    this.middlewares();
    this.loadCors();
    this.createsDatabaseConnection();
    this.routes();
  }

  private middlewares(): void {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private loadCors(): void {
    let corsOptions: cors.CorsOptions = {
      allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
      ],
      credentials: true,
      methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
      preflightContinue: false,
      origin: ['http://localhost:32750'],
      optionsSuccessStatus: 200,
    };

    this.express.use(cors(corsOptions));
  }

  private async createsDatabaseConnection(): Promise<void> {
    try {
      await createConnection();
    } catch (error) {
      console.log(error);
    }
  }

  private routes(): void {
    this.express.use('/api/', controllers);
  }
}

export default new App().express;