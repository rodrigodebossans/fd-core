import { GeneralException } from '@exception/general.exception';

export class UninformedObjectException extends GeneralException {
  constructor(cause?: any) {
    super(
      'UninformedObjectException',
      'Object properties have not been entered correctly, correct them',
      cause
    );
  }
}
