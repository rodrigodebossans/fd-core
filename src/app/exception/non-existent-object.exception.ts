import { GeneralException } from '@exception/general.exception';

export class NonExistentObjectException extends GeneralException {
  constructor(cause?: any) {
    super(
      'NonExistentObjectException',
      'The object you are looking for cannot be found in our database',
      cause
    );
  }
}
