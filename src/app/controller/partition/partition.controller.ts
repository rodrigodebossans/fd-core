import { Request, Response } from 'express';
import { Partition } from '@model/partition/partition';
import { NonExistentObjectException } from '@exception/non-existent-object.exception';
import partitionService from '@service/partition/partition.service';

class PartitionController {
  public async create(
    req: Request,
    res: Response
  ): Promise<Response<Partition>> {
    try {
      return res
        .status(200)
        .send(await partitionService.create(Partition.fromObj(req.body)));
    } catch (error) {
      return res.status(400).send({ error });
    }
  }

  public async listAll(
    req: Request,
    res: Response
  ): Promise<Response<Partition[]>> {
    try {
      return res
        .status(200)
        .send(Partition.fromArray(await partitionService.listAll()));
    } catch (error) {
      return res.status(400).send({ error });
    }
  }

  public async partitionByIdWithDirectoryTree(
    req: Request,
    res: Response
  ): Promise<Response<Partition>> {
    try {
      return res
        .status(200)
        .send(
          Partition.fromObj(
            await partitionService.listPartitionByIdWithDirectoryTree(
              parseInt(req?.params?.id)
            )
          )
        );
    } catch (error) {
      if (error instanceof NonExistentObjectException)
        return res.status(404).send({ error });

      return res.status(400).send({ error });
    }
  }

  public async edit(req: Request, res: Response): Promise<Response<Partition>> {
    try {
      return res
        .status(200)
        .send(
          await partitionService.edit(
            parseInt(req.params?.id),
            Partition.fromObj(req.body)
          )
        );
    } catch (error) {
      if (error instanceof NonExistentObjectException)
        return res.status(404).send({ error });

      return res.status(400).send({ error });
    }
  }

  public async delete(
    req: Request,
    res: Response
  ): Promise<Response<Partition>> {
    try {
      return res
        .status(200)
        .send(await partitionService.delete(parseInt(req.params?.id)));
    } catch (error) {
      if (error instanceof NonExistentObjectException)
        return res.status(404).send({ error });

      return res.status(400).send({ error });
    }
  }
}

export default new PartitionController();
