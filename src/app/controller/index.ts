import { Router } from 'express';
import partitionController from '@controller/partition/partition.controller';
import directoryController from '@controller/directory/directory.controller';

class Controllers {
  public routes = Router();

  constructor() {
    this.loadRoutes();
  }

  public getRoutes(): any {
    return this.routes;
  }

  public loadRoutes(): void {
    this.routes.get('/partition', partitionController.listAll);
    this.routes.get(
      '/partition/read-by-id-with-directory-tree/:id',
      partitionController.partitionByIdWithDirectoryTree
    );
    this.routes.post('/partition', partitionController.create);
    this.routes.put('/partition/:id', partitionController.edit);
    this.routes.delete('/partition/:id', partitionController.delete);

    this.routes.get('/directory', directoryController.listAll);
    this.routes.post('/directory', directoryController.create);
    this.routes.put('/directory/:id', directoryController.edit);
    this.routes.delete('/directory/:id', directoryController.delete);
  }
}

export default new Controllers().routes;
