import { Request, Response } from 'express';
import { NonExistentObjectException } from '@exception/non-existent-object.exception';
import { Directory } from '@model/directory/directory';
import directoryService from '@service/directory/directory.service';

class DirectoryController {
  public async create(
    req: Request,
    res: Response
  ): Promise<Response<Directory>> {
    try {
      return res
        .status(200)
        .send(await directoryService.create(Directory.fromObj(req.body)));
    } catch (error) {
      return res.status(400).send({ error });
    }
  }

  public async listAll(
    req: Request,
    res: Response
  ): Promise<Response<Directory[]>> {
    try {
      return res
        .status(200)
        .send(Directory.fromArray(await directoryService.listAll()));
    } catch (error) {
      return res.status(400).send({ error });
    }
  }

  public async edit(req: Request, res: Response): Promise<Response<Directory>> {
    try {
      return res.send(
        await directoryService.edit(
          parseInt(req.params?.id),
          Directory.fromObj(req.body)
        )
      );
    } catch (error) {
      if (error instanceof NonExistentObjectException)
        return res.status(404).send({ error });

      return res.status(400).send({ error });
    }
  }

  public async delete(
    req: Request,
    res: Response
  ): Promise<Response<Directory>> {
    try {
      return res
        .status(200)
        .send(
          Directory.fromObj(
            await directoryService.delete(parseInt(req.params?.id))
          )
        );
    } catch (error) {
      if (error instanceof NonExistentObjectException)
        return res.status(404).send({ error });

      return res.status(400).send({ error });
    }
  }
}

export default new DirectoryController();
