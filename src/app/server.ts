import app from './app';

class Server {
  constructor() {}

  public static start(): void {
    app.listen(3333, '0.0.0.0', () => {
      console.log(
        'The Fast Directories core module is running on port 3333...'
      );
    });
  }
}

Server.start();
