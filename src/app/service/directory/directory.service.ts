import { DirectoryRepository } from '@repository/directory/directory.repository';
import { getManager, getCustomRepository } from 'typeorm';
import { NonExistentObjectException } from '@exception/non-existent-object.exception';
import { Directory } from '@model/directory/directory';
class DirectoryService {
  public async create(directory: Directory): Promise<Directory> {
    await Directory.validate(directory);
    return await getManager().save(directory);
  }

  public async listAll(): Promise<Directory[]> {
    return await getManager().getTreeRepository(Directory).findTrees();
  }

  public async edit(id: number, directory: Directory): Promise<Directory> {
    await Directory.validate(directory);

    if (!(await getManager().getTreeRepository(Directory).findOne(id)))
      throw new NonExistentObjectException();

    delete directory.childDirectoryList;

    await getManager().update(Directory, id, directory);

    return await getManager().getTreeRepository(Directory).findOne(id);
  }

  public async delete(id: number): Promise<Directory> {
    const directory: Directory = await getManager()
      .getTreeRepository(Directory)
      .findOne(id);

    if (!directory) throw new NonExistentObjectException();
    else {
      const descendants: Directory[] = await getManager()
        .getTreeRepository(Directory)
        .findDescendants(directory);

      if (descendants?.length) {
        await getCustomRepository(DirectoryRepository).removeDirectoriesCascade(
          descendants
        );
      }

      await getManager().delete(Directory, directory);
      return directory;
    }
  }
}

export default new DirectoryService();
