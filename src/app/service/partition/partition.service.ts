import { DirectoryRepository } from '@repository/directory/directory.repository';
import { NonExistentObjectException } from '@exception/non-existent-object.exception';
import { Partition } from '@model/partition/partition';
import { getManager, getCustomRepository } from 'typeorm';
import { Directory } from '@model/directory/directory';

class PartitionService {
  public async create(partition: Partition): Promise<Partition> {
    await Partition.validate(partition);
    return await getManager().save(partition);
  }

  public async listAll(): Promise<Partition[]> {
    return await getManager()
      .getRepository(Partition)
      .find({ relations: ['directoryList'] });
  }

  public async listPartitionByIdWithDirectoryTree(
    id: number
  ): Promise<Partition> {
    const partition = await getManager()
      .getRepository(Partition)
      .findOne({ relations: ['directoryList'], where: { id } });

    if (!partition) throw new NonExistentObjectException();

    const promises = partition.directoryList.map(
      async (directory: Directory) =>
        await getManager()
          .getTreeRepository(Directory)
          .findDescendantsTree(directory)
    );

    await Promise.all(promises);
    partition.checkDirectoryListAndRemovesDuplicates();

    return partition;
  }

  public async edit(id: number, partition: Partition): Promise<Partition> {
    await Partition.validate(partition);

    if (!(await getManager().getRepository(Partition).findOne(id)))
      throw new NonExistentObjectException();

    await getManager()
      .createQueryBuilder()
      .update(Partition)
      .set({ name: partition.name })
      .where('id = :id', { id })
      .execute();

    return await getManager().getRepository(Partition).findOne(id);
  }

  public async delete(id: number): Promise<Partition> {
    const partition: Partition = await getManager()
      .getRepository(Partition)
      .findOne(id, { relations: ['directoryList'] });

    if (!partition) throw new NonExistentObjectException();
    else {
      if (partition.directoryList.length) {
        await getCustomRepository(DirectoryRepository).removeDirectoriesCascade(
          partition.directoryList
        );
      }
      await getManager().delete(Partition, id);
    }

    return partition;
  }
}

export default new PartitionService();
